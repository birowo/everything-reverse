// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"math/bits"
)

type Int interface {
	uint64 | uint32 | uint16 | uint8 | int64 | int32 | int16 | int8 | int
}

func reverseInt[T Int](x_ T) T {
	var x, y, rem uint64
	isNeg := x_ < T(0)
	if isNeg {
		x = uint64(-x_)
	} else {
		x = uint64(x_)
	}
	for x != 0 {
		x, rem = bits.Div64(0, x, 10)
		y = y*10 + rem
	}
	if isNeg {
		return -T(y)
	}
	return T(y)
}
func reverseSlice[T any](x []T) {
	var i, j int
	for i, j = 0, len(x)-1; i < j; i, j = i+1, j-1 {
		x[i], x[j] = x[j], x[i]
	}
}
func reverseString(str string) string {
	runes := []rune(str)
	reverseSlice(runes)
	return string(runes)
}
func reverseSliceKeep[T any](x []T) (y []T) { //tidak dipakai dikasus ini, pelengkap saja
	y = append(y, x...)
	var i, j int
	for i, j = 0, len(x)-1; i < j; i, j = i+1, j-1 {
		y[i], y[j] = y[j], y[i]
	}
	return
}
func reverseSliceFunc[T any](f func(T) T, x []T) { //tidak dipakai dikasus ini, pelengkap saja
	var i, j int
	for i, j = 0, len(x)-1; i < j; i, j = i+1, j-1 {
		x[i], x[j] = f(x[j]), f(x[i])
	}
	if i == j {
		x[i] = f(x[i])
	}
}
func reverseSliceKeepFunc[T any](f func(T) T, x []T) (y []T) {
	y = append(y, x...)
	var i, j int
	for i, j = 0, len(x)-1; i < j; i, j = i+1, j-1 {
		y[i], y[j] = f(y[j]), f(y[i])
	}
	if i == j {
		y[i] = f(y[i])
	}
	return
}
func main() {
	fmt.Println("Hello, 世界")

	println(reverseInt(uint64(1234567890123456789)))

	fmt.Println(reverseSliceKeepFunc(reverseInt[uint32], []uint32{1, 12, 123, 1234, 12345, 123456, 1234567, 12345678, 123456789}))

	fmt.Println(reverseSliceKeepFunc(reverseInt[int], []int{12, -23, 34, -45, 56}))

	println(reverseString("abcdefghijklmnopqrstuvwxyz"))

	fmt.Println(reverseSliceKeepFunc(reverseString, []string{"abc", "def", "ghi", "jkl", "mno", "Hello, 世界"}))
}
